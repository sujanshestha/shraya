<?php

session_start();
if(!isset($_SESSION['username'])) {
    header("Location: login.php");
}
include('includes/header.php');



if(isset($_GET['action']) && $_GET['action'] == 'delete' && $_GET['id'] != '') {
    $message = "user deleted Successfully";
    $sql = "DELETE from user where id ='".$_GET['id']."'";
    $result = $conn->query($sql);
    if ($conn->query($sql) === TRUE) {
        $_SESSION['status_message'] = 'user deleted successfully';
    } else {
        $_SESSION['status_message'] = 'Error occured';

    }
    echo "<script>window.location = 'user-manager.php';</script>";

}

if(isset($_POST['submit']) || isset($_GET['action']) && $_GET['action'] == 'delete' && $_GET['id'] != '') {

    if($_GET['action'] == 'edit' && $_GET['id'] != '') {
        $hobbies =  implode(",", $_POST['hobbies']);

        $sql = "UPDATE user SET `name` = '".$_POST['name']."',`email` = '".$_POST['email']."',`role` = '".$_POST['role']."',`hobbies` = '".$hobbies."', `bio` = '".$_POST['bia']."'  WHERE `id` = '".$_GET['id']."';";
        $message = "user Updated successfully";

    } elseif(isset($_GET['action']) && $_GET['action'] == 'delete' && $_GET['id'] != '') {
        $message = "user deleted Successfully";
        $sql = "DELETE from user where id ='".$_GET['id']."'";

    } else {
        $message = "user Created Successfully";

        $hobbies =  implode(",", $_POST['hobbies']);
        $sql = "INSERT INTO `user` (`name`,`email`,`role`, `hobbies`, `bio`) VALUES ('".$_POST['name']."','".$_POST['email']."','".$_POST['role']."','".$hobbies."', '".$_POST['bio']."')";
    }

    $result = $conn->query($sql);

    if ($conn->query($sql) === TRUE) {
        $_SESSION['status_message'] = $message;
    } else {
        $_SESSION['status_message'] = 'Error occured';

    }
    echo "<script>window.location = 'user-manager.php';</script>";

}

?>

<div class = 'clear'></div>


<?php if(isset($_GET['action']) && $_GET['action'] == 'add' || $_GET['action'] == 'edit' && $_GET['id'] != ''){ ?>

<?php
if($_GET['action'] == 'edit' && $_GET['id'] != '') {
    $sql = "SELECT * FROM user WHERE id = '".$_GET['id']."'";
    $result = $conn->query($sql);
    $user = $result->fetch_assoc();
    $hobbies = explode(',', $user['hobbies']);

}

 ?>

<div class = "content-form">

    <h3>Enter the user details</h3>

<form method="POST" action = "">
    <label for="name">user name</label>
    <input type="text" id="name" name="name" value="<?php echo ( isset($user['name']) &&  $user['name'] != '')?  $user['name']: ''; ?>">

    <label for="email">user email</label>
    <input type="text" id="email" name="email" value="<?php echo ( isset($user['email']) &&  $user['email'] != '')?  $user['email']: ''; ?>">

    <label for="role">Role</label>
    <select name="role">
        <option value="superadmin"  <?php if($user['role'] ==  'superadmin'){ ?> selected = "selected"<?php } ?>>Superadmin</option>
        <option value="admin" <?php if($user['role'] ==  'admin'){ ?> selected = "selected"<?php } ?>>Admin</option>
        <option value="user" <?php if($user['role'] ==  'user'){ ?> selected = "selected"<?php } ?>>User</option>
    </select>


<br/>
<br/>

    <label >Hobbies:</label>
     <label for="swimming">Swimming:</label> <input type="checkbox"  id = "swimming" name="hobbies[]" value="Swimming" <?php if(in_array('Swimming', $hobbies)){ ?> checked = "checked"<?php } ?>>
     <label for="football">Football:</label> <input type="checkbox"  id = "football"  name="hobbies[]" value="Football" <?php if(in_array('Football', $hobbies)){ ?> checked = "checked"<?php } ?>>
     <label for="voleyball">Volleyball:</label> <input type="checkbox"  id = "voleyball"  name="hobbies[]" value="Volleyball" <?php if(in_array('Volleyball', $hobbies)){ ?> checked = "checked"<?php } ?>>

    <label for="bio">Bio</label>
    <textarea id="bio" name="bio" ><?php echo ( isset($user['bio']) &&  $user['bio'] != '')?  $user['bio']: ''; ?></textarea>

    <input type="submit" value="Submit" name="submit">
</form>
</div>

<?php } else{ ?>

<div class="listing">
    <?php if(isset( $_SESSION['status_message'])){  echo  "<div>".$_SESSION['status_message']."</div>"; unset($_SESSION['status_message']); } ?>

<?php

    $user_sql = "SELECT * FROM user ";
    $user_result = $conn->query($user_sql);
 ?>

    <a href="?action=add">Add New user</a>

    <h3>user Listing:</h3>

    <table>
      <tr>
        <th>S.N.</th>
        <th>Bio</th>
        <th>Options</th>
      </tr>

        <?php
        $i= 1;
        if($result->num_rows > 0) {
        while ($user = $user_result->fetch_assoc()) {
            ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $user['bio'] ?></td>
            <td><a href="?action=edit&id=<?php echo $user['id']; ?>">Edit</a>|<a href="?action=delete&id=<?php echo $user['id']; ?>" onclick="return confirm('Are you sure to delete?');"> Delete</a></td>
        </tr>

            <?php
            $i++;
            }
        }
        ?>
    </table>
</div>

<?php } ?>


<?php include('includes/footer.php') ?>

