<?php

include('includes/connect.php');


if(isset($_SESSION['username'])) {
    header("Location: dashboard.php");
}
if(isset($_POST['submit'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];

    $sql = "SELECT * FROM admin WHERE email = '".$email."' and password = '".$password."'";
    $result = $conn->query($sql);

    if($result->num_rows > 0) {
        while ($admin_details = $result->fetch_assoc()) {
                session_start();
                $_SESSION['username'] = $admin_details['username'];
                header("Location: dashboard.php");
        }
    }
}

?>
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Admin login form</title>
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
  <form method = "POST" action = "">
  <h1>Admin login form</h1>
  <div class="inset">
  <p>
    <label for="email">Email</label>
    <input type="text" name="email" id="email">
  </p>
  <p>
    <label for="password">Password</label>
    <input type="password" name="password" id="password">
  </p>
  <p>
    <input type="checkbox" name="remember" id="remember">
    <label for="remember">Remember me</label>
  </p>
  </div>
  <p class="p-container">
    <span>Forgot password ?</span>
    <input type="submit" name="submit" id="submit" value="Log in">
  </p>
</form>

</body>
</html>
