<?php
session_start();
include('includes/connect.php');
if(!isset($_SESSION['username'])) {
    header("Location: login.php");
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Admin Dashboard</title>
      <link rel="stylesheet" href="css/admin-style.css">
      <script type="text/javascript" src = "js/jquery.js"></script>

</head>
<body>

<header>
    <div class="nav">
      <ul>
        <li class="home"><a class="active" href="dashboard.php">Dashboard</a></li>
        <li class="tutorials"><a href="page-manager.php">Page Manager</a></li>
        <li class="about"><a href="user-manager.php">User Manager</a></li>
        <li class="news"><a href="media-manager.php">Media Manager</a></li>
        <li class="contact"><a href="?action=logout">Log Out</a></li>
      </ul>
    </div>
</header>
