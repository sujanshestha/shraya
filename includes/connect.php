<?php

$servername = "localhost";
$username = "root";
$password = "root";
try {
    $conn = new mysqli($servername, $username, $password);
} catch (\Exception $e) {
    echo $e->getMessage(), PHP_EOL;
}
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
//check if database exists
if ($conn->select_db('shraya') === false) {
    $sql = "CREATE DATABASE shraya";
    if ($conn->query($sql) === TRUE) {
    echo "Database created.";
    echo "<br> ";
    } else {
    echo "Error: " . $conn->error;
    }
} else {
    // echo "database already exists.";
}


// create admin table
if ($result = $conn->query("SHOW TABLES LIKE 'admin'")) {
    if($result->num_rows == 0) {
        $create_admin_table = "CREATE TABLE admin (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        firstname VARCHAR(30) NOT NULL,
        lastname VARCHAR(30) NOT NULL,
        username VARCHAR(30) NOT NULL,
        password VARCHAR(30) NOT NULL,
        email VARCHAR(50)
        )";
        if ($conn->query($create_admin_table) === TRUE) {
        echo "admin table created ";
        echo "<br> ";
        } else {
        echo "Error" . $conn->error;
        }

        $table_exists = $conn->query('select 1 from `admin` LIMIT 1');
        if($table_exists !== FALSE) {

            $sql = "INSERT INTO admin (firstname, lastname, username, password, email)
            VALUES ('shraya', 'joshi', 'shraya', 'shraya', 'shraya@gmail.com')";
            if ($conn->query($sql) === TRUE) {
                echo "User created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }

        } else {

        }
    }
}
// create page table
if ($result = $conn->query("SHOW TABLES LIKE 'page'")) {
    if($result->num_rows == 0) {
        $create_page_table = "CREATE TABLE page (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        title VARCHAR(30) NOT NULL,
        image VARCHAR(255) NOT NULL,
        role VARCHAR(255) NOT NULL,
        hobbies VARCHAR(255) NOT NULL,
        content text
        )";
        if ($conn->query($create_page_table) === TRUE) {
        echo "page table created ";
        echo "<br> ";
        } else {
        echo "Error" . $conn->error;
        }
    }
}

// create user table// create user table
if ($result = $conn->query("SHOW TABLES LIKE 'user'")) {
    if($result->num_rows == 0) {
        $create_user_table = "CREATE TABLE user (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(30) NOT NULL,
        email VARCHAR(255) NOT NULL,
        role VARCHAR(255) NOT NULL,
        hobbies VARCHAR(255) NOT NULL,
        bio text
        )";
        if ($conn->query($create_user_table) === TRUE) {
        echo "user table created ";
        echo "<br> ";
        } else {
        echo "Error" . $conn->error;
        }
    }
}
// create user table


if(isset($_GET['action']) && $_GET['action'] == 'logout') {
    unset($_SESSION['username']);
     header("Location: login.php");
}
